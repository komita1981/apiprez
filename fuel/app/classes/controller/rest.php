<?php
/**
 * Main Rest Controller
 *
 * @package apiprez
 * @version 1.0
 * @author Milan Popovic
 * @copyright 2014 -  Foreach
 *
 */

namespace Controller;

use Controller_Rest;

use Uri;
use Request;

class Rest extends Controller_Rest
{
    /**
     * Module name
     *
     * @var string
     */
    protected $moduleName;

    /**
     * Required login indicator
     *
     * @var bool
     */
    protected $requireAuthentication = false;

    /**
     * Required https indicator
     *
     * @var bool
     */
    protected $requireHttps = false;

    /**
     * Response array container
     *
     * @var array
     */
    protected $responseArray = array();

    /**
     * Error message
     *
     * @var string
     */
    protected $errorMessage;

    /**
     * Authentication flag
     *
     * @var bool
     */
    protected $isAuthenticated;

    /**
     * Authenticated member
     *
     * @var Member
     */
    protected $member;

    /**
     * @inheritdoc
     */
    public function before()
    {
        $this->http_status = 200;
        $this->rest_format = 'json';

        $this->requireAuthentication();
        $this->requireHttps();

        parent::before();
    }

    /**
     * Constructor
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->moduleName = $request->module;
    }

    /**
     * Checks if member is logged on to system and breaks execution if not
     *
     * @return bool or exit
     */
    protected function requireAuthentication()
    {
        if ($this->requireAuthentication and ! $this->isAuthenticated)
        {
            exit;
        }
    }

    /**
     * Redirects member to secure connection
     */
    protected function requireHttps()
    {
        if ($this->requireHttps and  ! Uri::is_https())
        {
            exit;
        }
    }

    /**
     * Returns error with message
     *
     * @param $message
     *
     * @return object
     */
    protected function error($message)
    {
        return $this->response(array(
            'error' => $message
        ), $this->http_status);
    }

    /**
     * Returns error if user is not privileged
     *
     * @return mixed
     */
    protected function notPrivileged()
    {
        return $this->noWayHere(403);
    }

    /**
     * Returns error that user cant get what he requested
     *
     * @param int $http_status
     *
     * @return mixed
     */
    protected function noWayHere($http_status = 400)
    {
        return $this->response(array(
            'error' => __('No way here :-)', $this->moduleName)
        ), $http_status);
    }

    /**
     * Returns error that user is invalid
     *
     * @param int $http_status
     *
     * @return mixed
     */
    protected function invalidMemberError($http_status = 400)
    {
        return $this->response(array(
            'error' => __('Invalid user', $this->moduleName)
        ), $http_status);
    }

    /**
     * Returns response
     *
     * @return object
     */
    protected function returnResponse()
    {
        return $this->response($this->responseArray, $this->http_status);
    }

    /**
     * Returns last segment of original uri
     *
     * @return mixed
     */
    protected function getLastSegment()
    {
        return $this->request->uri->segment(count($this->request->uri->segments()));
    }

    /**
     * Member validation check
     *
     * @param $memberId
     *
     * @return bool
     */
    protected function checkMember($memberId)
    {
        return true;
    }

    /**
     * Response
     *
     * Takes pure data and optionally a status code, then creates the response
     *
     * @param   array $data
     * @param   int $httpStatus
     *
     * @return  object  Response instance
     */
    protected function response($data = array(), $httpStatus = null)
    {
        // set the correct response header
        if (method_exists('Format', 'to_'.$this->format))
        {
            $this->response->set_header('Content-Type', $this->_supported_formats[$this->format]);
        }

        // no data returned? Set the NO CONTENT status on the response
        if ((is_array($data) and empty($data)) or ($data == ''))
        {
            $this->response->status = $this->no_data_status;
            return $this->response;
        }

        // make sure we have a valid return status
        $httpStatus or $httpStatus = $this->http_status;

        // If the format method exists, call and return the output in that format
        if (method_exists('Format', 'to_'.$this->format))
        {
            // Handle XML output
            if ($this->format === 'xml')
            {
                // Detect basenode
                $xmlBasenode = $this->xml_basenode;
                $xmlBasenode or $xmlBasenode = \Config::get('rest.xml_basenode', 'xml');

                // Set the XML response
                $this->response->body(\Format::forge($data)->{'to_'.$this->format}(null, null, $xmlBasenode));
            }
            else
            {
                // Set the formatted response
                $this->response->body(\Format::forge($data)->{'to_'.$this->format}());
            }
        }

        // Format not supported, but the output is an array
        elseif (is_array($data))
        {
                // convert it to json so we can at least read it while we're developing
                $this->response->body(\Format::forge($data)->to_array(null, true));
        }

        // Format not supported, output directly
        else
        {
            $this->response->body($data);
        }

        // Set the reponse http status
        $httpStatus and $this->response->status = $httpStatus;

        return $this->response;
    }
}