<?php
/**
 * Post Rest Controller
 *
 * @package apiprez
 * @subpackage member
 * @version 1.0
 * @author Milan Popovic
 * @copyright 2014 -  Foreach
 *
 */

namespace Post;

use Input;
use Event;
use Exception;
use Log;
use Controller\Rest;

class Controller_Rest_V1_Post extends Rest
{
    /**
     * Post service instance
     *
     * @var PostService
     */
    protected $postService;

    /**
     * Member service instance
     *
     * @var MemberService
     */
    protected $memberService;

    /**
     * @inheritdoc
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->postService = Service_Locator::getService('postService');
    }

    /**
     * Returns single post data
     *
     * @return object
     */
    public function get_index()
    {
        $postId = Input::secured_get('id');
        try
        {
            $this->response_array = $this->prepare_response($this->postService->getPost($postId));
        }
        catch (Exception_Post_Not_Exists $e)
        {
            $this->http_status = 404;

            return $this->error($e->getMessage());
        }
        catch (Exception_Post_Deleted $e)
        {
            $this->http_status = 410;

            return $this->error($e->getMessage());
        }
        catch (Exception_Permission_Denied $e)
        {
            $this->http_status = 403;

            return $this->error($e->getMessage());
        }
        catch (Exception $e)
        {
            $this->http_status = 500;

            $this->informAdmin('postGet', array($this->member, $postId));

            return $this->error(__('Internal error. Please try later', $this->moduleName));
        }

        return $this->return_response();
    }

    /**
     * Puts new post
     *
     * @return object
     */
    public function post_index()
    {
        try
        {
            if (! $this->postService->addPost($this->getPostInput(), $this->member))
            {
                $this->http_status = 400;
                
                return $this->error(__('Post is not saved', $this->moduleName));
            }

            $this->response_array = $this->prepare_response($this->postService->getPost());

            return $this->return_response();
        }
        catch (Exception_Permission_Denied $e)
        {
            $this->http_status = 403;

            return $this->error(__('You can not post here', $this->moduleName));
        }
        catch (Exception_Invalid_Post_Input $e)
        {
            $this->http_status = 400;

            return $this->error($e->getMessage());
        }
        catch (Exception $e)
        {
        }

        $this->informAdmin('postInsert', array($this->member, $this->getPostInput()));

        return $this->error(__('Internal error. Please try later', $this->moduleName));
    }
    /**
     * Delete post
     *
     * @return object
     */
    public function delete_index()
    {
        $postId = $this->getLastSegment();
        try{
            $this->postService->delete($postId, $this->member);
        }
        catch (Exception_Permission_Denied $e)
        {
            $this->http_status = 403;

            return $this->error(__('You can not delete post', $this->moduleName));
        }
        catch (Exception_Invalid_Post_Input $e)
        {
            $this->http_status = 400;

            return $this->error($e->getMessage());
        }
        catch (Exception $e)
        {
            $this->informAdmin('postDelete', array($this->member, $postId));

            return $this->error(__('Internal error. Please try later', $this->moduleName));

        }

        $this->responseArray['postId'] = $postId;

        return $this->return_response();
    }

    /**
     * Forwards some post (like retweet)
     *
     * @return object
     */
    public function put_repost()
    {
        $postId = Input::secured_put('id');
        try{
            $this->postService->repost($this->member, $postId);
        }
        catch (Exception_Permission_Denied $e)
        {
            $this->http_status = 403;

            return $this->error(__('You can not repost', $this->moduleName));
        }
        catch (Exception_Invalid_Post_Input $e)
        {
            $this->http_status = 400;

            return $this->error($e->getMessage());
        }
        catch (Exception $e)
        {
            $this->informAdmin('postRepost', array($this->member, $postId));

            return $this->error(__('Internal error. Please try later', $this->moduleName));

        }


        return $this->return_response();
    }

    /**
     * Prepares API response for provided post
     *
     * @param Post $post
     *
     * @return array
     */
    protected function prepare_response(Post $post)
    {
        $response = array();
        foreach ($this->postService->getPostOutputFields() as $field)
        {
            $response[$field] = $post->$field;
        }

        $response['memberCreator'] = $this->memberService->getMember($post->memberId);
        $response['permalink'] = $this->postService->getPermalink($post);
        $response['likeDislike'] = $this->postService->getLikeDislike();

        return $response;
    }
}