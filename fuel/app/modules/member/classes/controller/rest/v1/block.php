<?php
/**
 * Block rest controller
 *
 * @package apiprez
 * @subpackage member
 * @version 1.0
 * @author Milan Popovic
 * @copyright 2014 -  Foreach
 *
 */

namespace Member;

use Controller\Rest;

use Input;

class Controller_Rest_V1_Block extends Rest
{
    /**
     * Block service instance
     * 
     * @var BlockService
     */
    protected $blockService;

    /**
     * @inheritdoc
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->blockService = Service_Locator::getService('blockService');
    }

    /**
     * Action for blocking provided member from authenticated member
     *
     * @return object
     */
    public function post_index()
    {
        $memberId = Input::secure_json('memberId');
        
        if (! $this->checkMember($memberId))
        {
            return $this->invalidMemberError();
        }

        if ($this->blockService->isAlreadyBlocked($this->member->getId(), $memberId))
        {
            $this->http_status = 409;

            return $this->error(__('Member is already blocked', $this->moduleName));
        }

        if (! $this->blockService->blockMember($this->member->getId(), $memberId))
        {
            $this->http_status = 500;
            $this->informAdmin('block', array($this->member, $memberId));
            
            return $this->error(__('Internal error. Please try later', $this->moduleName));
        }

        $this->responseArray['message'] = __('Member is blocked', $this->moduleName);

        return $this->returnResponse();
    }

    /**
     * Remove block by authenticated user from user with id memberId
     *
     * @return object
     */
    public function delete_index()
    {
        $memberId = Input::secured_delete('memberId');
        if (! $this->checkMember($memberId))
        {
            return $this->invalidMemberError();
        }

        if (! $this->blockService->checkMemberBlock($this->member->getId(), $memberId))
        {
            $this->http_status = 404;

            return $this->error(__('Member is not blocked anymore', $this->moduleName));
        }

        if (! $this->blockService->removeBlock($this->member->getId(), $memberId))
        {
            $this->http_status = 500;
            $this->informAdmin('blockRemove', array($this->member, $memberId));

            return $this->error(__('Internal error. Please try later', $this->moduleName));
        }

        $this->response_array['message'] = __('Member is not block anymore', $this->moduleName);

        return $this->returnResponse();
    }
}