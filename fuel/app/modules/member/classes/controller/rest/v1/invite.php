<?php
/**
 * Invite rest controller
 *
 * @package apiprez
 * @subpackage member
 * @version 1.0
 * @author Milan Popovic
 * @copyright 2014 -  Foreach
 *
 */

namespace Member;

use Controller\Rest;
use Input;
use Validation;

class Controller_Rest_V1_Invite extends Rest
{
    /**
     * Invitation service instance
     *
     * @var InvitationService
     */
    protected $invitationService;

    /**
     * Member service instance
     *
     * @var MemberService
     */
    protected $memberService;

    /**
     * @inheritdoc
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->invitationService = Service_Locator::getService('invitationService');
        $this->memberService = Service_Locator::getService('member');
    }
    
    /**
     * Sends invitation to provided email addresses
     *
     * @return object
     */
    public function post_index()
    {
        $validation         = Validation::forge();

        $emails = Input::secured_post('emails');

        if (! is_array($emails))
        {
            $this->http_status = 400;

            return $this->error(__('Invalid emails provided', $this->moduleName));
        }

        foreach ($emails as $index => $email)
        {
            $this->responseArray[$index] = array(
                'email'         => $email,
                'invited'       => false,
                'error' => '',
                'member'        => null);

            if (! $validation->validateEmail($email))
            {
                $this->responseArray[$index]['error'] = __('Invalid email', $this->moduleName);

                continue;
            }

            if ($this->invitationService->memberExists($email))
            {
                $this->responseArray[$index]['error'] = __('Member already registered', $this->moduleName);
                $this->responseArray[$index]['member'] = $this->memberService->getMemberByEmail($email);

                continue;
            }

            if ($this->invitationService->isAlreadySent($email))
            {
                $this->responseArray[$index]['error'] = __('Invitation already sent', $this->moduleName);

                continue;
            }

            if ($this->invitationService->send($email, $this->member->getId()))
            {
                $this->responseArray[$index]['invited'] = true;

                continue;
            }

            $this->informAdmin('invitation', array($emails));

            return $this->error(__('Internal error. Please try later', $this->moduleName));
        }

        return $this->returnResponse();
    }
}