<?php
/**
 * Friend request rest controller
 *
 * @package apiprez
 * @subpackage member
 * @version 1.0
 * @author Milan Popovic
 * @copyright 2014 -  Foreach
 *
 */

namespace Member;

use Controller\Rest;
use Input;
use Event;
use Log;

class Controller_Rest_V1_Friends extends Rest
{
    /**
     * Friends service instance
     *
     * @var FriendsService
     */
    protected $friendsService;

    /**
     * @inheritdoc
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->friendsService = Service_Locator::getService('friendsService');
    }

    /**
     * Return friends list
     *
     * @return object
     */
    public function get_list()
    {
        $start = (int) Input::secured_get('offset');
        $limit = (int) Input::secured_get('limit');

        $this->responseArray = $this->friendsService->getFriends($this->member->getId(), $start, $limit);

        return $this->returnResponse();
    }

    /**
     * Return mutual friends list
     *
     * @return object
     */
    public function get_mutual()
    {
        $start = (int) Input::secured_get('offset');
        $limit = (int) Input::secured_get('limit');

        $this->responseArray = $this->friendsService->getMutualFriends($this->member->getId(), $start, $limit);

        return $this->returnResponse();
    }

    /**
     * Creates friendships between members
     *
     * @return object
     */
    public function post_index()
    {
        $memberId          = Input::secured_json('memberId');

        if (! $this->checkMember($memberId))
        {
            return $this->invalidMemberError();
        }
        
        if (! $this->friendsService->createFriendship($this->member->getId(), $memberId))
        {
            $this->http_status = $this->friendsService->getStatus();

            return $this->error($this->friendsService->errors());
        }
        
        $this->informAdmin('friendshipCreate', array($this->member, $memberId));

        return $this->error(__('Internal error. Please try later', $this->moduleName));

        return $this->return_response();

    }

    /**
     * Removes friendship between members
     *
     * @return object
     */
    public function delete_index()
    {
        $memberId          = Input::secured_json('memberId');
        
        if (! $this->checkMember($memberId))
        {
            return $this->invalidMemberError();
        }

        if (! $this->friendsService->friendshipExists($this->member->getId(), $memberId))
        {
            $this->http_status = 404;

            return $this->error(__('Friendship does not exists', $this->moduleName));
        }

        if (! $this->friendsService->breakFriendship($this->member->getId(), $memberId))
        {
            $this->http_status = 500;

            $this->informAdmin('friendshipBreak', array($this->member, $memberId));

            return $this->error(__('Internal error. Please try later', $this->moduleName));
        }

        $this->response_array['message'] = __('Friendship broken', $this->moduleName);

        return $this->returnResponse();
    }
}